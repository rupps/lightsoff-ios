//
//  GameScene.swift
//  Lights OFF
//
//  Created by Александр on 14.09.14.
//  Copyright (c) 2014 Rupps. All rights reserved.
//

import SpriteKit

let screenSize: CGRect = UIScreen.mainScreen().bounds

class GameScene: SKScene {
	
	let _gridWidth = screenSize.width
	let _gridHeigh = screenSize.width
	
	let _gridLowerLeftCorner: CGPoint = CGPoint(x: 10, y: (screenSize.height - screenSize.width) / 2)
	
	let _margin: CGFloat = 10
	
	
	var level = 0;
	var _numRows: CGFloat = 5;
	
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
		setScene()
		startPlaying()
		
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
		
        /*for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            let sprite = SKSpriteNode(imageNamed:"Spaceship")
            
            sprite.xScale = 0.5
            sprite.yScale = 0.5
            sprite.position = location
            
            let action = SKAction.rotateByAngle(CGFloat(M_PI), duration:1)
            
            sprite.runAction(SKAction.repeatActionForever(action))
            
            self.addChild(sprite)
        }*/
    }
	
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
	
	func startPlaying() {
		
		if (level == 1) {
			_numRows = 4
		} else if (level == 6 || level > 12) {
			_numRows = 6
		}
		
		let tileSize = calculateTileSize()
		
		var numBlocks = 0
		if (level > 1) {
			numBlocks = level / 2 - 1 + Int(arc4random_uniform(2))
		}
		
		
		for r in 0..<Int(_numRows) {
			for c in 0..<Int(_numRows) {
				let tilePos = getTilePosition(row: r, cell: c)
				let tile = SKShapeNode(rect: CGRectMake(tilePos.x, tilePos.y, tileSize.width, tileSize.height))
				tile.fillColor = SKColor.grayColor()
				self.addChild(tile)
			}
		}
	}
	
	func calculateTileSize() -> CGSize {
		let tileWidth = _gridWidth / _numRows - _margin
		
		return CGSize(width: tileWidth, height: tileWidth)
	}
	
	func getTileAtPosition(xPos x: Int, yPos y: Int)  {}
	
	func getTilePosition(row r:Int, cell c: Int) -> CGPoint {
		let tileSize = calculateTileSize()
		let x = Int(_gridLowerLeftCorner.x) + Int(_margin / 2) + (c * (Int(tileSize.width) + Int(_margin / 2)))
		let y = Int(_gridLowerLeftCorner.y) + Int(_margin / 2) + (r * (Int(tileSize.width) + Int(_margin / 2)))
		return CGPoint(x: x, y: y)
	}
	

	
	func setScene() {
		self.backgroundColor = SKColor.whiteColor()
	}
}
